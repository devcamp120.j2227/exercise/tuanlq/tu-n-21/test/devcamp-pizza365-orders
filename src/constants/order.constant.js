export const ON_CHANGE_SELECT_ROW_NUMBER = "Khi người dúng ấn nút thay đổi số dòng";

export const ON_SELECT_NUMBER_PAGE = "khi người dùng chon trang";

export const ON_LOADING_ORDER_PENDING = "Bắt đầu tải trang load order";

export const ON_LOADING_ORDER_SUCCESS = "Lấy dữ liệu order thành công";

export const ON_LOADING_ORDER_ERROR  = "Lấy dữ liệu order thất bại";

export const ON_CLICK_OPEN_MODAL_EDIT_ORDER = "Mở cửa sổ edit order";

export const ON_LOADING_DRINK_SUCCESS = "lấy dữ liệu đồ uống thành công";

export const ON_LOADING_DRINK_ERROR = "Lấy dữ liệu đồ uống thất bại";

export const ON_CHANGE_ORDER_STATUS = "Khi khách hàng chọn trạng thái đơn hàng";

export const ON_EDIT_ORDER_PENDING = "Bắt đầu edit order ";

export const ON_EDIT_ORDER_SUCCESS = "Edit order thành công";

export const ON_EDIT_ORDER_ERROR = "Edit order thất bại";

export const CLOSE_EDIT_ORDER_MODAL = "Close Edit order modal";