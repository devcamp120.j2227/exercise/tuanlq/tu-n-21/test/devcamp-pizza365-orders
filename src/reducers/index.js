import { combineReducers } from "redux";
import orderReducer from "./orders.reducer";

const rootReducer = combineReducers({
    orderReducer
})
export default rootReducer;