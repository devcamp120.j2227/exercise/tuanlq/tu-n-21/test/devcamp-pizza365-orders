import { 
    ON_CHANGE_SELECT_ROW_NUMBER,
    ON_SELECT_NUMBER_PAGE,
    ON_LOADING_ORDER_PENDING,
    ON_LOADING_ORDER_SUCCESS,
    ON_LOADING_ORDER_ERROR,
    ON_CLICK_OPEN_MODAL_EDIT_ORDER,
    ON_LOADING_DRINK_SUCCESS,
    ON_LOADING_DRINK_ERROR,
    ON_CHANGE_ORDER_STATUS,
    ON_EDIT_ORDER_PENDING,
    ON_EDIT_ORDER_SUCCESS,
    ON_EDIT_ORDER_ERROR,
    CLOSE_EDIT_ORDER_MODAL
} from "../constants/order.constant";


const initialState = {
    pending: false,
    pageRows: 10,
    NoPage: 1,
    NumberOfPage: 2,
    orders: [],
    orderSplitPage: [],
    ordersInOnePage: [],
    showModalEdit: false,
    orderDetail: {
        id: 132906,
        orderCode: "",
        kichCo: "none",
        duongKinh: "",
        suon: 0,
        salad: "",
        loaiPizza: "none",
        idVourcher: "",
        thanhTien: 0,
        giamGia: 0,
        idLoaiNuocUong: "none",
        soLuongNuoc: 0,
        hoTen: "",
        email: "",
        soDienThoai: "",
        diaChi: "",
        loiNhan: "",
        trangThai: "none",
        ngayTao: 0,
        ngayCapNhat: 0
    },
    drink:[],
    objectTrangThai: {
        trangThai:""
    }
}


const orderReducer = (state = initialState, action) => {
    switch(action.type) {
        case ON_CHANGE_SELECT_ROW_NUMBER:
            state.NoPage = 1;
            state.orderSplitPage = [];
            state.pageRows = action.payload;
            state.NumberOfPage = Math.ceil(state.orders.length / state.pageRows);
            //chia nhỏ order theo số hàng (chunks)
            for (let i = 0; i < state.orders.length; i += state.pageRows ) {
                const chunk = state.orders.slice(i, i + state.pageRows);
                state.orderSplitPage.push(chunk);
            }
            state.ordersInOnePage = state.orderSplitPage[state.NoPage - 1];
            break;
        case ON_SELECT_NUMBER_PAGE: 
            state.NoPage = action.payload;
            state.ordersInOnePage = state.orderSplitPage[state.NoPage - 1];
            break;
        case ON_LOADING_ORDER_PENDING:
            state.pending = true;
            break;
        case ON_LOADING_ORDER_SUCCESS:
            state.pending = false;
            state.orders = action.payload;
            state.NumberOfPage = Math.ceil(state.orders.length / state.pageRows);
            //chia nhỏ order theo số hàng (chunks)
            for (let i = 0; i < state.orders.length; i += state.pageRows ) {
                const chunk = state.orders.slice(i, i + state.pageRows);
                state.orderSplitPage.push(chunk);
            }
            state.ordersInOnePage = state.orderSplitPage[state.NoPage - 1];
            break;
        case ON_LOADING_ORDER_ERROR:
            alert("Server is not working any more!");
            break;
        case ON_CLICK_OPEN_MODAL_EDIT_ORDER:
            state.showModalEdit = true;
            state.orderDetail = action.payload;
            if(state.orderDetail.kichCo === "S"){
                state.orderDetail.duongKinh = 20;
                state.orderDetail.suon = 2;
                state.orderDetail.salad = 200;
                state.orderDetail.soLuongNuoc = 2;
                state.orderDetail.thanhTien = 150000;
            }
            if(state.orderDetail.kichCo === "M"){
                state.orderDetail.duongKinh = 25;
                state.orderDetail.suon = 4;
                state.orderDetail.salad = 300;
                state.orderDetail.soLuongNuoc = 3;
                state.orderDetail.thanhTien = 200000;
            }
            if(state.orderDetail.kichCo === "L"){
                state.orderDetail.duongKinh = 30;
                state.orderDetail.suon = 8;
                state.orderDetail.salad = 500;
                state.orderDetail.soLuongNuoc = 4;
                state.orderDetail.thanhTien = 250000;
            }
            break;
        case ON_LOADING_DRINK_SUCCESS:
            state.drink = action.payload;
            break;
        case ON_LOADING_DRINK_ERROR:
            break;
        case ON_CHANGE_ORDER_STATUS:
            state.orderDetail.trangThai = action.payload;
            state.objectTrangThai.trangThai = action.payload;
            break;
        case ON_EDIT_ORDER_PENDING:
            break;
        case ON_EDIT_ORDER_SUCCESS:
            alert("Thay đổi trạng thái order thành công!")
            state.showModalEdit = false;
            break;
        case ON_EDIT_ORDER_ERROR:
            alert("Thay đổi trạng thái order thất bại!");
            console.log(action.payload)
            state.showModalEdit = false;
            break;
        case CLOSE_EDIT_ORDER_MODAL:
            state.showModalEdit = false;
            break;
        default: 
        break;
    }
    return {...state};
}

export default orderReducer;