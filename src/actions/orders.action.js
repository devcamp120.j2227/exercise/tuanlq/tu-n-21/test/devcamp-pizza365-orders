import { 
    ON_CHANGE_SELECT_ROW_NUMBER , 
    ON_LOADING_ORDER_ERROR, 
    ON_LOADING_ORDER_PENDING, 
    ON_LOADING_ORDER_SUCCESS, 
    ON_SELECT_NUMBER_PAGE,
    ON_CLICK_OPEN_MODAL_EDIT_ORDER,
    ON_LOADING_DRINK_SUCCESS,
    ON_LOADING_DRINK_ERROR,
    ON_CHANGE_ORDER_STATUS,
    ON_EDIT_ORDER_PENDING,
    ON_EDIT_ORDER_SUCCESS,
    ON_EDIT_ORDER_ERROR,
    CLOSE_EDIT_ORDER_MODAL
} from "../constants/order.constant";
import axios from "axios";

const  URL = "http://203.171.20.210:8080/devcamp-pizza365/orders";
export const onChangeSelectRowNumber = (value) =>{
    return{
        type: ON_CHANGE_SELECT_ROW_NUMBER,
        payload: value
    }
} 

export const onSelectNumberPage = (value) => {
    return{
        type: ON_SELECT_NUMBER_PAGE,
        payload: value
    }
}

export const requestOrderData = () => {
    return async (dispatch) => {
        try{
            var requestOption = {
                method: "GET",
                redirect: "follow"
            };
            await dispatch({
                type: ON_LOADING_ORDER_PENDING,
            })

            const responseOrder = await fetch(URL, requestOption);

            const data = await responseOrder.json();

            await dispatch ({
                type: ON_LOADING_ORDER_SUCCESS,
                payload: data
            })
        }catch (error){
            return({
                type: ON_LOADING_ORDER_ERROR,
                error: error
            })
        }
    }
}

export const openModalEditOrder = (value) => {
    return{
        type: ON_CLICK_OPEN_MODAL_EDIT_ORDER,
        payload: value
    }
}

export const loadDrinkData = (value) => {
    return async(dispatch) => {
        try{ 
            
            var requestOption = {
            method: "GET",
            redirect: "follow"
            };

            const responseOrder = await fetch("http://203.171.20.210:8080/devcamp-pizza365/drinks", requestOption);

            const data = await responseOrder.json();

            await dispatch ({
                type: ON_LOADING_DRINK_SUCCESS,
                payload: data
            })
        }catch (error){
            return({
                type: ON_LOADING_DRINK_ERROR,
                error: error
            })
        }
    }
}

export const changeOrderStatus = (value) => {
    return{
        type: ON_CHANGE_ORDER_STATUS,
        payload: value
    }
}

export const editOrder = (objectStatus, objectid) => {
    return async (dispatch ) => {
        dispatch({
            type: ON_EDIT_ORDER_PENDING
        })
        axios({
            method: "put",
            url: URL + "/" + objectid,
            data: JSON.stringify(objectStatus),
            headers: {
                "Content-type": "application/json;charset=UTF-8"
            }
        })
        .then(data =>{
            dispatch({
                type: ON_EDIT_ORDER_SUCCESS, 
                payload: data
            })
        })
        .catch(error => {
            dispatch({
                type: ON_EDIT_ORDER_ERROR,
                error: error
            })
        })
    }
}

export const  closeModalEditOrder = () => {
    return {
        type: CLOSE_EDIT_ORDER_MODAL
    }
}