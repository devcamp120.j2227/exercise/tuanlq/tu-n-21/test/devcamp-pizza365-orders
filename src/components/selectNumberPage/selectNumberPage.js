import { useSelector, useDispatch } from "react-redux";
import { FormControl, Select, MenuItem  } from "@mui/material";
import { onChangeSelectRowNumber } from "../../actions/orders.action";


export default function SelectNumberPage (){
    const dispatch = useDispatch();
    const {pageRows} = useSelector ((reduxData) => reduxData.orderReducer);
    const handleChange = (event) => {
        dispatch(onChangeSelectRowNumber(event.target.value))
    }
    return(
        <>
            <FormControl variant="standard" sx={{ m: 1, minWidth: 120 }}>
                <Select
                value={pageRows}
                onChange={handleChange}
                label="Rows"
                >
                <MenuItem value={5}>5</MenuItem>
                <MenuItem value={10}>10</MenuItem>
                <MenuItem value={25}>25</MenuItem>
                <MenuItem value={50}>50</MenuItem>
                </Select>
            </FormControl>
        </>
    )
}