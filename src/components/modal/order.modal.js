import Modal from '@mui/material/Modal';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import { Grid ,TextField, MenuItem, FormControl, Select, Button, InputLabel, } from '@mui/material';
import { useDispatch, useSelector } from "react-redux";
import { useEffect } from 'react';
import { loadDrinkData, changeOrderStatus, editOrder, closeModalEditOrder } from '../../actions/orders.action';


export default function ModalEditOrder (){
    const dispatch = useDispatch();
    const {showModalEdit, orderDetail, drink} = useSelector((redux) => redux.orderReducer);

    const style = {
        position: 'absolute',
        width: 400,
        bgcolor: 'background.paper',
        boxShadow: 24,
        p: 4,
        marginBottom:" 10px",
        borderRadius: "10px"
    };
    const modalStyle1 = {
        position:'absolute',
        top: "100px",
        overflow:'scroll',
        height:'1000px',
        display:"flex",
        justifyContent: "center"
      };
    
    const onChangeStatus = (event) => {
        dispatch(changeOrderStatus(event.target.value));
    }

    const handlerEditOrder = () => {
        dispatch(editOrder(orderDetail, orderDetail.id))
    }
    const handlerCloseModal = (event) => {
        dispatch(closeModalEditOrder());
    }
    useEffect(()=>{
        dispatch(loadDrinkData());
    },[dispatch])
    return (
        <div>
            <Modal
            keepMounted
            sx={modalStyle1}
            open = {showModalEdit}
            onClose = {handlerCloseModal}
            aria-labelledby="keep-mounted-modal-title"
            aria-describedby="keep-mounted-modal-description"
            >
            <Box sx={style}>
                <Grid container>
                    <Grid item lg={12} md={12} sm={12} xs={12} mt={3}>
                        <Typography variant="h4"mb={2}>Edit Order: {orderDetail.orderCode}</Typography>
                    </Grid>
                    <Grid item lg={4} md={4} sm={4} xs={4} mt={3}>
                        <Typography >Order code </Typography>
                    </Grid>

                    <Grid item lg={8} md={8} sm={8} xs={8} mt={3} sx={{width: "100%"}}>
                        <TextField value={orderDetail.orderCode} sx={{width: "100%"}} size="small" disabled></TextField>
                    </Grid>
                    <Grid item lg={4} md={4} sm={4} xs={4} mt={3} >
                        <Typography >Cỡ combo </Typography>
                    </Grid>

                    <Grid item lg={8} md={8} sm={8} xs={8} mt={3} sx={{width: "100%"}}>
                    <FormControl sx={{width: "100%"}} size="small">
                        <InputLabel >Combo</InputLabel>
                            <Select 
                            disabled
                            value={orderDetail.kichCo}
                            label="Combo"
                            > 
                                <MenuItem value="none" selected>Chọn Combo ...</MenuItem>
                                <MenuItem value="S">Nhỏ</MenuItem>
                                <MenuItem value="M">Vừa</MenuItem>
                                <MenuItem value="L">Lớn</MenuItem>
                            </Select>
                        </FormControl>
                    </Grid>

                    <Grid item lg={4} md={4} sm={4} xs={4} mt={3}>
                        <Typography >Đường kính pizza </Typography>
                    </Grid>
                    <Grid item lg={8} md={8} sm={8} xs={8} mt={3} sx={{width: "100%"}}>
                        <TextField value={orderDetail.duongKinh} sx={{width: "100%"}} size="small" disabled></TextField>
                    </Grid>

                    <Grid item lg={4} md={4} sm={4} xs={4} mt={3}>
                        <Typography >Sườn nướng </Typography>
                    </Grid>
                    <Grid item lg={8} md={8} sm={8} xs={8} mt={3} sx={{width: "100%"}}>
                        <TextField value={orderDetail.suon} sx={{width: "100%"}} size="small" disabled></TextField>
                    </Grid>

                    <Grid item lg={4} md={4} sm={4} xs={4} mt={3}>
                        <Typography >Số lượng nước uống </Typography>
                    </Grid>
                    <Grid item lg={8} md={8} sm={8} xs={8} mt={3} sx={{width: "100%"}}>
                        <TextField value={orderDetail.soLuongNuoc} sx={{width: "100%"}} size="small" disabled></TextField>
                    </Grid>

                    <Grid item lg={4} md={4} sm={4} xs={4} mt={3}>
                        <Typography >Salad</Typography>
                    </Grid>
                    <Grid item lg={8} md={8} sm={8} xs={8} mt={3} sx={{width: "100%"}}>
                        <TextField value={orderDetail.salad} sx={{width: "100%"}} size="small" disabled></TextField>
                    </Grid>

                    <Grid item lg={4} md={4} sm={4} xs={4} mt={3}>
                        <Typography >Thành tiền</Typography>
                    </Grid>
                    <Grid item lg={8} md={8} sm={8} xs={8} mt={3} sx={{width: "100%"}}>
                        <TextField value={orderDetail.thanhTien} sx={{width: "100%"}} size="small" disabled></TextField>
                    </Grid>

                    <Grid item lg={4} md={4} sm={4} xs={4} mt={3}>
                        <Typography>Đồ uống </Typography>
                    </Grid>
                    <Grid item lg={8} md={8} sm={8} xs={8} mt={3}>
                        <FormControl sx={{width: "100%"}} size="small">
                        <InputLabel >Drink</InputLabel>
                            <Select
                            disabled
                            value={orderDetail.idLoaiNuocUong}
                            label="Drink"
                            > 
                                <MenuItem value="none">None</MenuItem>
                                {drink.map((element, index)=>{
                                    return (
                                        <MenuItem value={element.maNuocUong} key={index}>{element.tenNuocUong}</MenuItem>
                                    )
                                })}
                            </Select>
                        </FormControl>
                    </Grid>

                    <Grid item lg={4} md={4} sm={4} xs={4} mt={3}>
                        <Typography >Voucher Id </Typography>
                    </Grid>
                    <Grid item lg={8} md={8} sm={8} xs={8} mt={3} sx={{width: "100%"}}>
                        <TextField value={orderDetail.idVourcher} sx={{width: "100%"}} size="small"></TextField>
                    </Grid>

                    <Grid item lg={4} md={4} sm={4} xs={4} mt={3}>
                        <Typography>Loai pizza</Typography>
                    </Grid>
                    <Grid item lg={8} md={8} sm={8} xs={8} mt={3}>
                        <FormControl sx={{width: "100%"}} size="small">
                        <InputLabel >Pizza</InputLabel>
                            <Select value={ orderDetail.loaiPizza.toUpperCase()} 
                            label="Register Status"
                             disabled>
                                <MenuItem value="NONE">None</MenuItem>
                                <MenuItem value="SEAFOOD">Hải sản</MenuItem>
                                <MenuItem value="HAWAII">Hawaii</MenuItem>
                                <MenuItem value="BACON">Thị hun khói</MenuItem>
                            </Select>
                        </FormControl>
                    </Grid>

                    <Grid item lg={4} md={4} sm={4} xs={4} mt={3}>
                        <Typography >Giảm giá</Typography>
                    </Grid>
                    <Grid item lg={8} md={8} sm={8} xs={8} mt={3} sx={{width: "100%"}}>
                        <TextField value={orderDetail.giamGia} sx={{width: "100%"}} size="small" disabled></TextField>
                    </Grid>

                    <Grid item lg={4} md={4} sm={4} xs={4} mt={3}>
                        <Typography >Họ và tên</Typography>
                    </Grid>
                    <Grid item lg={8} md={8} sm={8} xs={8} mt={3} sx={{width: "100%"}}>
                        <TextField value={orderDetail.hoTen} sx={{width: "100%"}} size="small" disabled></TextField>
                    </Grid>

                    <Grid item lg={4} md={4} sm={4} xs={4} mt={3}>
                        <Typography >Email</Typography>
                    </Grid>
                    <Grid item lg={8} md={8} sm={8} xs={8} mt={3} sx={{width: "100%"}}>
                        <TextField value={orderDetail.email} sx={{width: "100%"}} size="small" disabled></TextField>
                    </Grid>

                    <Grid item lg={4} md={4} sm={4} xs={4} mt={3}>
                        <Typography >Số điện thoại</Typography>
                    </Grid>
                    <Grid item lg={8} md={8} sm={8} xs={8} mt={3} sx={{width: "100%"}}>
                        <TextField value={orderDetail.soDienThoai} sx={{width: "100%"}} size="small" disabled></TextField>
                    </Grid>

                    <Grid item lg={4} md={4} sm={4} xs={4} mt={3}>
                        <Typography >Địa chỉ</Typography>
                    </Grid>
                    <Grid item lg={8} md={8} sm={8} xs={8} mt={3} sx={{width: "100%"}}>
                        <TextField value={orderDetail.diaChi} sx={{width: "100%"}} size="small" disabled></TextField>
                    </Grid>

                    <Grid item lg={4} md={4} sm={4} xs={4} mt={3}>
                        <Typography >Trạng thái đơn hàng </Typography>
                    </Grid>
                    <Grid item lg={8} md={8} sm={8} xs={8} mt={3}>
                        <FormControl sx={{width: "100%"}} size="small">
                            <InputLabel >Trạng thái</InputLabel>
                                <Select 
                                value={orderDetail.trangThai}
                                label="Trạng thái"
                                onChange={onChangeStatus}
                                > 
                                    <MenuItem value="none" >Trạng thái...</MenuItem>
                                    <MenuItem value="open">Open</MenuItem>
                                    <MenuItem value="cancel">Đã huỷ</MenuItem>
                                    <MenuItem value="confirmed">Đã xác nhận</MenuItem>
                                </Select>
                        </FormControl>
                    </Grid>
                    <Grid item lg={12} md={12} sm={12} xs={12} mt={5} sx={{display: "flex", justifyContent:"end"}}>
                        <Button   variant='contained' color="success" onClick={handlerEditOrder}>Edit Order</Button>
                        &ensp;<Button   variant='contained' color="error" onClick={handlerCloseModal}>Huỷ bỏ</Button>
                    </Grid>
                </Grid>
            </Box>
            </Modal>
        </div>
    );
}