import { CircularProgress, tableCellClasses , Container, Grid, Pagination, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Typography , Button} from "@mui/material"
import { useDispatch, useSelector } from "react-redux";
import { styled } from '@mui/material/styles';
import SelectNumberPage from "./selectNumberPage/selectNumberPage";
import { onSelectNumberPage , requestOrderData, openModalEditOrder } from "../actions/orders.action";
import EditIcon from '@mui/icons-material/Edit';
import { useEffect } from "react";
import ModalEditOrder from "./modal/order.modal";

//Edit style Row
const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
      backgroundColor: theme.palette.common.black,
      color: theme.palette.common.white,
      fontSize: "18px",

    },
    [`&.${tableCellClasses.body}`]: {
      fontSize: 14,
    },
  }));
  
const StyledTableRow = styled(TableRow)(({ theme }) => ({
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
    // hide last border
    '&:last-child td, &:last-child th': {
      border: 0,
    },
  }));
export default function Order(){
    const dispatch = useDispatch();
    const {NoPage, NumberOfPage , ordersInOnePage, pending, orders} = useSelector((reduxData) => reduxData.orderReducer);
    const  handlerEditOrder = (value) => {
        dispatch(openModalEditOrder(value));
    };
    //hàm thay đổi thông tin bảng khi chọn số trang
    const onChangePagination = (event,value) => {
        dispatch(onSelectNumberPage(value));
    };
    useEffect(() => {
        dispatch(requestOrderData());
    },[dispatch]);
    return(
        <>
        <ModalEditOrder></ModalEditOrder>
        {pending === true ? 
            <Container sx={{display:"flex" , justifyContent:" Center", alignItems:"Center", height:"1000px"}}>
                <CircularProgress></CircularProgress>
            </Container>
            :
            <Container>
                <Grid container mt={5}> 
                    <Grid item lg={12} md={12} sm={12}  sx={{display: "flex", justifyContent:"center"}}>
                        <h1>Danh Sách Đơn Hàng</h1>
                    </Grid>
                    <Grid item lg={12} md={12} sm={12} xs={12}>
                        <TableContainer component={Paper}>
                            <Table sx={{ minWidth: 650 }} aria-label="simple table">
                                <TableHead>
                                <StyledTableRow>
                                    <StyledTableCell  align="center">Order ID</StyledTableCell>
                                    <StyledTableCell  align="center">Kích cỡ Combo</StyledTableCell>
                                    <StyledTableCell  align="center">Loại Pizza</StyledTableCell>
                                    <StyledTableCell  align="center">Nước Uống</StyledTableCell>
                                    <StyledTableCell  align="center">Thành Tiền</StyledTableCell>
                                    <StyledTableCell  align="center">Họ và Tên</StyledTableCell>
                                    <StyledTableCell  align="center">Số Điện Thoại</StyledTableCell>
                                    <StyledTableCell  align="center">Trạng Thái</StyledTableCell>
                                    <StyledTableCell  align="center">Action</StyledTableCell>
                                </StyledTableRow>
                                </TableHead>
                                <TableBody>
                                    {ordersInOnePage.map((element, index) => {
                                        return(
                                            <StyledTableRow key={index}>
                                                <StyledTableCell  align="center">{element.orderCode}</StyledTableCell>
                                                <StyledTableCell  align="center">{element.kichCo}</StyledTableCell>
                                                <StyledTableCell  align="center">{element.loaiPizza}</StyledTableCell>
                                                <StyledTableCell  align="center">{element.idLoaiNuocUong}</StyledTableCell>
                                                <StyledTableCell  align="center">{element.thanhTien}</StyledTableCell>
                                                <StyledTableCell  align="center">{element.hoTen}</StyledTableCell>
                                                <StyledTableCell  align="center">{element.soDienThoai}</StyledTableCell>
                                                <StyledTableCell  align="center">{element.trangThai}</StyledTableCell>
                                                <StyledTableCell  align="center">
                                                    <Button onClick={() => handlerEditOrder(element)} variant='contained' color="primary" size="small">
                                                        <EditIcon fontSize="small"></EditIcon>&ensp;Edit
                                                    </Button>
                                                    &ensp;
                                                </StyledTableCell>
                                            </StyledTableRow>
                                            )
                                        })
                                    } 
                                </TableBody>
                            </Table>
                        </TableContainer>
                    </Grid>
                    <Grid item lg={6} md={6} sm={6} xs={6} mt={5}>
                        <Pagination count={NumberOfPage} page={NoPage}  onChange={onChangePagination} />
                    </Grid>
                    <Grid item lg={4} md={4} sm={4} xs={4} mt ={5} sx={{display: "flex", justifyContent:"end"}}>
                        <Typography>Row numbers:</Typography>
                    </Grid>
                    <Grid item lg={2} md={2} sm={2} xs={2} mt ={3}  sx={{display: "flex", justifyContent:"end"}}>
                        <SelectNumberPage></SelectNumberPage>
                    </Grid>
                </Grid>
            </Container>
        }
        </>
    )
}